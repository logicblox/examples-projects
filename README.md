Basic LogicBlox Project and Module Example
==========================================

This example shows how to restructure the code used in the "LogiQL in 30 minutes" tutorial into a LogicBlox project structure.

How to build
------------

We'll assume [you have Vagrant installed](https://developer.logicblox.com/2013/11/develop-using-vagrant/).

To build and test the example clone its repository:

    $ hg clone https://bitbucket.org/logicblox/examples-projects

Then link or copy in a LogicBlox release tarball into the checkout directory. For instance, if you have one downloaded into your `~/Downloads` folder:

    $ cd examples-projects
    $ ln ~/Downloads/logicblox-4.0.5.tar.gz

Then, boot up vagrant and SSH into the virtual machine:

    $ vagrant up
    $ vagrant ssh

To compile the project run the following inside of the VM:

    $ lb config && make

This will compile the project, but not yet create a workspace with the predicates in there. To create the workspace and load the libraries into it, run:

    $ make check-ws-application

To verify that it worked, print the contents of the `core:profit:agg_profit` predicate:

    $ lb print application core:profit:agg_profit